import re
from collections import ChainMap

STRIP_RE = re.compile(r'\D')

def normalize_number(n,country_prefix):
    n = n.strip()
    if n.startswith('+'):
        n = '00' + n[1:]
    n = STRIP_RE.sub('',n)
    if n.startswith(country_prefix):
        n = '0' + n[4:]

    return n

def build_name(firstname, lastname):
    if firstname and lastname:
        return ' '.join((lastname, firstname))
    if lastname:
        return lastname
    return firstname


class DeepChainMap(ChainMap):
  def __init__(self, *maps, **map0):
    super().__init__(*filter(None, [map0] + list(maps)))
  def __getattr__(self, k):
    k_maps = list()
    for m in self.maps:
      if k in m:
        if isinstance(m[k], dict): k_maps.append(m[k])
        else: return m[k]
    if not k_maps: raise AttributeError(k)
    return DeepChainMap(*k_maps)
  def __setattr__(self, k, v):
    if k in ['maps']: return super().__setattr__(k, v)
    self[k] = v
