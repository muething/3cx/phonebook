from lxml import etree
from aiohttp import web
import itertools

from .util import normalize_number, build_name


class YealinkPhoneBook:

    def __init__(self,config):
        self.labels = dict(
            company = 'Firma',
            extension = 'Nebenstelle',
            mobile = 'Mobil',
            work = 'Geschäftlich',
            private = 'Privat',
            external = 'Extern',
        )
        self.row = None
        self.entry = None
        self.split_entries = bool(config.yealink.get('split_entries',True))
        self.normalize_numbers = bool(config.yealink.get('normalize_numbers',True))
        self.country_prefix = config.yealink.get('country_prefix','0049')
        self.include_external = bool(config.yealink.get('include_external',True))

        self.pb = etree.Element('XXXIPPhoneDirectory',attrib=dict(clearlight='true'))
        title = etree.SubElement(self.pb,'Title')
        title.text = 'Phonelist'
        prompt = etree.SubElement(self.pb,'Prompt')
        prompt.text = 'Prompt'


    def make_entry(self,suffix = None):
        entry = etree.SubElement(self.pb,'DirectoryEntry')
        name = etree.SubElement(entry,'Name')
        name.text = build_name(self.row['firstname'],self.row['lastname'])
        if suffix:
            name.text = ' '.join((name.text,suffix))
        if self.row.get('company',None):
            company = etree.SubElement(entry,'Extra',attrib=dict(label=self.labels['company']))
            company.text = self.row['company'].strip()
        return entry

    def add_entry(self,row):
        self.row = row
        if not self.split_entries:
            self.entry = self.make_entry()


    def add_number(self, kind, no_suffix=False):

        data = self.row.get(kind,None)

        if data:

            if self.split_entries:
                entry = self.make_entry(suffix=None if no_suffix else self.labels[kind])
            else:
                entry = self.entry

            number = etree.SubElement(entry,'Telephone',attrib=dict(label=self.labels[kind]))
            if self.normalize_numbers:
                data = normalize_number(data, self.country_prefix)
            number.text = data


    def __call__(self, phonebook, extensions):

        for row in sorted(itertools.chain(phonebook,extensions),key=lambda e : (e['lastname'],e['firstname'])):

            self.add_entry(row)

            self.add_number('extension', no_suffix=True)
            self.add_number('mobile')
            self.add_number('work')
            self.add_number('private')
            if self.include_external:
                self.add_number('external')

        return web.Response(
            body=etree.tostring(self.pb,pretty_print=True,encoding='utf-8',xml_declaration=True),
            charset='utf-8',
            content_type='text/xml',
        )
