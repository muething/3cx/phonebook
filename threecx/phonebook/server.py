import asyncio
import asyncpg
import yaml
from ssl import SSLContext, VerifyMode
from aiohttp import web
from http_basic_auth import parse_header, BasicAuthException
from .yealink import YealinkPhoneBook
from .util import DeepChainMap


async def handle_phonebook_request(request, build_phonebook):

    # Extract the user's extension from the HTTP authorization header
    try:
        extension, password = parse_header(request.headers.get("Authorization", ""))
    except BasicAuthException:
        raise web.HTTPUnauthorized(headers={"WWW-Authenticate": "Basic"})

    pool = request.app["db-pool"]

    async with pool.acquire() as conn:

        async with conn.transaction():

            # Make sure that the extension exists and the request contains the correct password
            cred = await conn.fetchrow(
                "SELECT dn.iddn AS dn, extension.authpswd AS password FROM dn INNER JOIN extension ON dn.iddn = extension.fkiddn WHERE dn.value = $1",
                extension,
            )
            if not cred or password != cred["password"]:
                raise web.HTTPUnauthorized(headers={"WWW-Authenticate": "Basic"})
            dn = cred["dn"]

            phonebook_query = await conn.prepare(
                """
SELECT
  CASE WHEN fkiddn = $1 THEN 'private' ELSE 'public' END AS source,
  lastname,
  firstname,
  company,
  phonenumber AS mobile,
  pv_an1 AS private,
  pv_an3 AS work
FROM
  phonebook
WHERE
  fkiddn IS NULL OR fkiddn = $1
ORDER BY
  lastname, firstname
"""
            )

            # Get all entries from both the public and the user-specific phonebook
            phonebook = await phonebook_query.fetch(dn)

            # Get all extensions that have not been hidden. This data is distributed over a whole lot of places,
            # making for a rather more involved SQL query, as the information about whether the extension should
            # be hidden and the extension's mobile number are stored in different rows of the same table.
            extension_query = await conn.prepare(
                """
SELECT
  'extension' as source,
  dn.value AS extension,
  extension.outcid AS external,
  users.lastname AS lastname,
  users.firstname AS firstname,
  dnp_mobile.value AS mobile
FROM
  users
INNER JOIN
  extension ON users.fkidextension = extension.fkiddn
INNER JOIN
  dn ON users.fkidextension = dn.iddn
INNER JOIN
  dnprop AS dnp_show ON dnp_show.fkiddn = users.fkidextension
INNER JOIN
  dnprop AS dnp_mobile ON dnp_mobile.fkiddn = users.fkidextension
WHERE
  extension.enabled = TRUE
AND
  dnp_show.name = 'DONT_SHOW_EXT_IN_PHBK'
AND
  dnp_show.value = '0'
AND
  dnp_mobile.name = 'MOBILENUMBER'
ORDER BY
  lastname, firstname
"""
            )

            extensions = await extension_query.fetch()

    return build_phonebook(phonebook, extensions)


def make_handler(build_phonebook_factory, config):
    def handle(request):
        return handle_phonebook_request(request, build_phonebook_factory(config))

    return handle


runners = []


async def init_app(config):
    app = web.Application()

    if config.db.ssl:
        if config.db.verify_certificate:
            ssl = True
        else:
            ssl = SSLContext()
            ssl.verify_mode = VerifyMode.CERT_NONE
    else:
        ssl = False

    app["db-pool"] = await asyncpg.create_pool(
        user=config.db.user,
        password=config.db.password,
        database=config.db.database,
        host=config.db.host,
        ssl=ssl,
        min_size=1,
        max_size=10,
        max_inactive_connection_lifetime=120,
    )

    app.router.add_route(
        "GET", "/yealink/phonebook.xml", make_handler(YealinkPhoneBook, config)
    )

    runner = web.AppRunner(app)
    runners.append(runner)
    await runner.setup()
    site = web.TCPSite(runner, "localhost", config.server.port)
    await site.start()
    print("Site running")


def main():

    with open("/etc/3cx/phonebook.yaml") as configfile:
        config = DeepChainMap(yaml.safe_load(configfile))

    loop = asyncio.get_event_loop()
    # app = loop.run_until_complete(init_app(config, loop))
    # web.run_app(app, port=int(config.server.port))
    loop.create_task(init_app(config))

    try:
        loop.run_forever()
    finally:
        for runner in runners:
            loop.run_until_complete(runner.cleanup())
