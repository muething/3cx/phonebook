from setuptools import setup, PEP420PackageFinder
from os import path

here = path.abspath(path.dirname(__file__))

# Get the long description from the README file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()

setup(
    name='3cx-utils',  # Required

    use_scm_version=True,

    description='A server that generates better IP phone phonebooks for the 3CX VoIP PBX.',  # Required

    long_description=long_description,  # Optional

    long_description_content_type='text/markdown',  # Optional (see note above)

    #url='https://gitlab.com/muething/3cx-utils',  # Optional

    author='Steffen Müthing',  # Optional

    author_email='steffen.muething@muething.com',  # Optional

    classifiers=[  # Optional
        'Development Status :: 4 - Beta',

        'Intended Audience :: Developers',

        'License :: OSI Approved :: BSD License',

        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],

    # keywords='Docker',  # Optional

    packages=PEP420PackageFinder.find(),  # Required

    setup_requires=[
        'setuptools_scm',
    ],

    install_requires=[
        'aiohttp',
        'asyncpg',
        'lxml',
        'http_basic_auth',
        'PyYAML',
    ],  # Optional

    extras_require={  # Optional
    },

    python_requires=">=3.6",

    package_data={  # Optional
    },

    # data_files=[('my_data', ['data/data_file'])],  # Optional

    entry_points={  # Optional
        'console_scripts': [
            '3cx-phonebookserver=threecx.phonebook.server:main',
        ],
    },

    #project_urls={  # Optional
    #    'Bug Reports': 'https://gitlab.dune-project.org/infrastructure/markdownfilter/issues',
    #    'Source': 'https://gitlab.dune-project.org/infrastructure/markdownfilter',
    #},
)
