# GitLab Merge Request Commit Message Filter

This project contains a simple pandoc filter written in Python to improve the formatting
of the automatically generated merge request commit messages in GitLab.
